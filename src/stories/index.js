import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';

import ChangeRoleButton from '../components/ChangeRoleButton';

storiesOf('Welcome', module).add('to Storybook', () => <Welcome showApp={linkTo('Button')} />);

storiesOf('Button', module)
  .add('Acurable Role', () => <ChangeRoleButton role='Acurable' appearance='primary' changeRole={action('clicked')}>Hello Button</ChangeRoleButton>)
  .add('Organisation Role', () => <ChangeRoleButton role='Organisation' changeRole={action('clicked')}>Hello Button</ChangeRoleButton>);
