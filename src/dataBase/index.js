import loki from 'lokijs';

const SENSOR_DEVICES = [
    { name: 'Phone', status: 'assigned', state: 'ready', id: 1, type: 'sensor', created: 2017 },
    { name: 'Phone', status: 'inventory', state: 'ready', id: 2, type: 'sensor', created: 2018 },
    { name: 'Phone', status: 'assigned', state: 'active', id: 3, type: 'sensor', created: 2018 },
    { name: 'Phone', status: 'assigned', state: 'ready', id: 4, type: 'sensor', created: 2017 },
    { name: 'Phone', status: 'inventory', state: 'ready', id: 5, type: 'sensor', created: 2018 },
    { name: 'Phone', status: 'assigned', state: 'ready', id: 6, type: 'sensor', created: 2018 },
    { name: 'Phone', status: 'inventory', state: 'ready', id: 7, type: 'sensor', created: 2017 },
    { name: 'Phone', status: 'assigned', state: 'ready', id: 8, type: 'sensor', created: 2018 },
    { name: 'Phone', status: 'inventory', state: 'ready', id: 9, type: 'sensor', created: 2017 },
    { name: 'Phone', status: 'inventory', state: 'ready', id: 10, type: 'sensor', created: 2018 },
    { name: 'Phone', status: 'inventory', state: 'ready', id: 11, type: 'sensor', created: 2018 },
    { name: 'Phone', status: 'assigned', state: 'ready', id: 12, type: 'sensor', created: 2017 },
    { name: 'Phone', status: 'assigned', state: 'ready', id: 13, type: 'sensor', created: 2017 },
    { name: 'Phone', status: 'assigned', state: 'active', id: 14, type: 'sensor', created: 2017 },
    { name: 'Phone', status: 'assigned', state: 'active', id: 15, type: 'sensor', created: 2017 },
    { name: 'Phone', status: 'assigned', state: 'active', id: 16, type: 'sensor', created: 2017 },
    { name: 'Phone', status: 'inventory', state: 'ready', id: 17, type: 'sensor', created: 2018 },
    { name: 'Phone', status: 'inventory', state: 'ready', id: 18, type: 'sensor', created: 2018 },
    { name: 'Phone', status: 'inventory', state: 'ready', id: 19, type: 'sensor', created: 2017 },
    { name: 'Phone', status: 'inventory', state: 'ready', id: 20, type: 'sensor', created: 2018 },
];

const RESEIVER_DEVICES = [
    { name: 'Phone', status: 'assigned', state: 'ready', id: 1, type: 'receiver', created: 2017 },
    { name: 'Phone', status: 'inventory', state: 'ready', id: 2, type: 'receiver', created: 2017 },
    { name: 'Phone', status: 'assigned', state: 'active', id: 3, type: 'receiver', created: 2017 },
    { name: 'Phone', status: 'assigned', state: 'ready', id: 4, type: 'receiver', created: 2017 },
    { name: 'Phone', status: 'inventory', state: 'ready', id: 5, type: 'receiver', created: 2017 },
    { name: 'Phone', status: 'assigned', state: 'ready', id: 6, type: 'receiver', created: 2018 },
    { name: 'Phone', status: 'inventory', state: 'ready', id: 7, type: 'receiver', created: 2018 },
    { name: 'Phone', status: 'assigned', state: 'ready', id: 8, type: 'receiver', created: 2018 },
    { name: 'Phone', status: 'inventory', state: 'ready', id: 9, type: 'receiver', created: 2018 },
    { name: 'Phone', status: 'inventory', state: 'disabled', id: 10, type: 'receiver', created: 2018 },
    { name: 'Phone', status: 'inventory', state: 'ready', id: 11, type: 'receiver', created: 2018 },
    { name: 'Phone', status: 'assigned', state: 'ready', id: 12, type: 'receiver', created: 2018 },
    { name: 'Phone', status: 'assigned', state: 'ready', id: 13, type: 'receiver', created: 2018 },
    { name: 'Phone', status: 'assigned', state: 'active', id: 14, type: 'receiver', created: 2018 },
    { name: 'Phone', status: 'assigned', state: 'active', id: 15, type: 'receiver', created: 2018 },
    { name: 'Phone', status: 'assigned', state: 'active', id: 16, type: 'receiver', created: 2018 },
    { name: 'Phone', status: 'inventory', state: 'ready', id: 17, type: 'receiver', created: 2018 },
    { name: 'Phone', status: 'inventory', state: 'ready', id: 18, type: 'receiver', created: 2018 },
    { name: 'Phone', status: 'inventory', state: 'ready', id: 19, type: 'receiver', created: 2018 },
    { name: 'Phone', status: 'inventory', state: 'ready', id: 20, type: 'receiver', created: 2017 },
]

const LOGS = [
    { date: '2018-09-12', description: '10 sensor devices assigned', user: 'Acurable' },
    { date: '2018-08-01', description: '1 receiver device assigned', user: 'Organisation' },
    { date: '2018-07-12', description: '3 receiver devices assigned', user: 'Acurable' },
    { date: '2018-07-04', description: '12 sensor devices assigned', user: 'Organisation' },
    { date: '2018-06-18', description: '5 receiver devices assigned', user: 'Acurable' },
    { date: '2018-06-15', description: '7 sensor devices assigned', user: 'Organisation' },
    { date: '2018-06-12', description: '2 sensor devices assigned', user: 'Acurable' },
]

let db = new loki('example.db');
let sensorDevices = db.addCollection('sensorDevices');
let receiverDevices = db.addCollection('receiverDevices');
let logsCollection = db.addCollection('logsCollection');

export const dB = () => {
    SENSOR_DEVICES.map(device => (
        sensorDevices.insert(device)
    ));
    RESEIVER_DEVICES.map(device => (
        receiverDevices.insert(device)
    ));
    LOGS.map(workLog => (
        logsCollection.insert(workLog)
    ));

    return {
        sensorDevices: sensorDevices.data,
        receiverDevices: receiverDevices.data,
        logsCollection: logsCollection.data,
    };
}
