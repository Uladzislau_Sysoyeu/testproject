import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import SensorDevices from '../../components/SensorDevices'
import ReceiverDevices from '../../components/ReceiverDevices'
import LogsInfo from '../../components/LogsInfo';

import ChangeRoleButton from '../../components/ChangeRoleButton';

import { requestConnectToDB, requestChangeRole } from '../../redux/details';

import { ContentWrapper, PageTitle, TitleWrapper } from './styled';

class Devices extends Component {
  static contextTypes = {
    requestConnectToDB: PropTypes.func,
    requestChangeRole: PropTypes.func,
    sensorList: PropTypes.array,
    receiverList: PropTypes.array,
    role: PropTypes.string,
  };

  state = {
    isOrganisation: false,
  }

  componentDidMount() {
    this.props.requestConnectToDB();
    this.props.requestChangeRole('Acurable');
  }

  changeRole = () => {
    this.setState({ isOrganisation: !this.state.isOrganisation })
    this.props.requestChangeRole(this.state.isOrganisation ? 'Acurable' : 'Organisation');
  }

  render() {
    const { sensorList, receiverList, role, logsList } = this.props;
    const { isOrganisation } = this.state;
    return (
      <ContentWrapper>
        <TitleWrapper>
          <PageTitle><h1>Devices</h1></PageTitle>
          <ChangeRoleButton
            role={role}
            appearance={isOrganisation ? null : "primary"}
            changeRole={this.changeRole} 
          />
        </TitleWrapper>
        {
          sensorList &&
          <SensorDevices
            role={role}
            total={sensorList.length}
            created={sensorList.filter(device => device.created === 2018).length}
            ready={sensorList.filter(device => device.state === 'ready').length}
            active={sensorList.filter(device => device.state === 'active').length}
            disable={sensorList.filter(device => device.state === 'disabled').length}
          />
        }
        {
          receiverList &&
          <ReceiverDevices
            role={role}
            list={receiverList}
            total={receiverList.length}
            created={receiverList.filter(device => device.created === 2018).length}
            ready={receiverList.filter(device => device.state === 'ready').length}
            active={receiverList.filter(device => device.state === 'active').length}
            disable={receiverList.filter(device => device.state === 'disabled').length}
          />
        }
        {logsList &&
          <LogsInfo
            list={logsList}
            role={role}
          />
        }
      </ContentWrapper >
    );
  }
}

const mapStateToProps = ({ details }) => ({
  sensorList: details.collectionsList.sensorDevices,
  receiverList: details.collectionsList.receiverDevices,
  logsList: details.collectionsList.logsCollection,
  role: details.role
});

const mapDispatchToProps = {
  requestConnectToDB,
  requestChangeRole,
};

export default connect(mapStateToProps, mapDispatchToProps)(Devices);
