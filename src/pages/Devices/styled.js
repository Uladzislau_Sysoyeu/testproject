import styled from 'styled-components';
import { gridSize } from '@atlaskit/theme';

export const ContentWrapper = styled.div`
  margin: ${gridSize() * 4}px ${gridSize() * 8}px;
  padding-bottom: ${gridSize() * 3}px;
`;

export const TitleWrapper = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
`;

export const PageTitle = styled.div`
  margin-bottom: ${gridSize() * 2}px;
`;

export const Role = PageTitle.extend`

`
