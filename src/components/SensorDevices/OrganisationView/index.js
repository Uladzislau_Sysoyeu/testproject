import React from 'react';
import Button, { ButtonGroup } from '@atlaskit/button';

import {
    Container,
    HeaderWrapper,
    TableText,
    TableNumbersText,
    SensorDevicesWrapper,
    BlockData,
    BlockDataStatistics,
} from '../styled';

const Organisation = ({ total, active, disable }) => (
    <div>
        <Container>
            <HeaderWrapper><h3>Sensor devices</h3></HeaderWrapper>
            <ButtonGroup>
                <Button appearance="primary">Manage sensors</Button>
            </ButtonGroup>
        </Container>
        <SensorDevicesWrapper>
            <BlockData>
                <BlockDataStatistics>
                    <TableText><h6>In-stock</h6></TableText>
                    <TableNumbersText>{total}</TableNumbersText>
                </BlockDataStatistics>
                <BlockDataStatistics>
                    <TableText><h6>Active</h6></TableText>
                    <TableNumbersText>{active}</TableNumbersText>
                </BlockDataStatistics>
                <BlockDataStatistics>
                    <TableText><h6>Disabled</h6></TableText>
                    <TableNumbersText>{disable}</TableNumbersText>
                </BlockDataStatistics>
            </BlockData>
        </SensorDevicesWrapper>
    </div>
);


export default Organisation;
