import PropTypes from 'prop-types';
import React from 'react';
import { Link } from 'react-router';
import Nav, {
  AkContainerTitle,
  AkNavigationItem,
} from '@atlaskit/navigation';
import SearchIcon from '@atlaskit/icon/glyph/search';
import CreateIcon from '@atlaskit/icon/glyph/add';
import AtlassianIcon from '@atlaskit/icon/glyph/atlassian';

import atlaskitLogo from '../images/atlaskit.png';

export default class StarterNavigation extends React.Component {
  state = {
    navLinks: [
      ['/organisations', 'Organisations'],
      ['/sites', 'HealthCare Sites'],
      ['/users', 'Users'],
      ['/devices', 'Devices'],
      ['/studies', 'Studies'],
      ['/billing', 'Billing'],
      ['/service', 'Service Plan'],
    ]
  };

  static contextTypes = {
    navOpenState: PropTypes.object,
    router: PropTypes.object,
  };

  render() {
    const globalPrimaryIcon = <AtlassianIcon label="Atlassian icon" size="xlarge" />;

    return (
      <Nav
        isOpen={this.context.navOpenState.isOpen}
        width={this.context.navOpenState.width}
        onResize={this.props.onNavResize}
        containerHeaderComponent={() => (
          <AkContainerTitle
            href="https://atlaskit.atlassian.com/"
            icon={
              <img alt="atlaskit logo" src={atlaskitLogo} />
            }
            text="AcuNoea"
          />
        )}
        globalPrimaryIcon={globalPrimaryIcon}
        globalPrimaryItemHref="/"
        globalSearchIcon={<SearchIcon label="Search icon" />}
        hasBlanket
        globalCreateIcon={<CreateIcon label="Create icon" />}
      >
        {
          this.state.navLinks.map(link => {
            const [url, title] = link;
            return (
              <Link key={url} to={url}>
                <AkNavigationItem
                  text={title}
                  isSelected={this.context.router.isActive(url, true)}
                />
              </Link>
            );
          }, this)
        }
      </Nav>
    );
  }
}
