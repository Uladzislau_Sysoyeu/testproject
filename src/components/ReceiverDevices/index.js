import React from 'react';

import Acurable from './AccurableView';
import Organisation from './OrganisationView';

const ReceiverDevices = ({ total, ready, active, disable, created, role }) => (
    role === 'Acurable' ?
        <Acurable
            total={total}
            ready={ready}
            active={active}
            disable={disable}
            created={created}
        /> :
        <Organisation
            total={total}
            active={active}
            disable={disable}
        />
);


export default ReceiverDevices;
