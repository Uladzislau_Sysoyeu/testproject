import React from 'react';
import Button, { ButtonGroup } from '@atlaskit/button';

import {
    Container,
    HeaderWrapper,
    TitleWrapper,
    Title,
    TableText,
    TableNumbersText,
    ReceiverDevicesWrapper,
    BlockData,
    BlockDataStatistics,
    ReceiverWrapper,
} from '../styled';

const Acurable = ({ total, ready, active, disable, created, role }) => (
    <ReceiverWrapper>
        <Container>
            <HeaderWrapper><h3>Receiver devices</h3></HeaderWrapper>
            <ButtonGroup>
                <Button>Inventory</Button>
                <Button appearance="primary">Manage sensors</Button>
            </ButtonGroup>
        </Container>
        <TitleWrapper>
            <Title>Inventory</Title>
            <Title>Assigned</Title>
        </TitleWrapper>
        <ReceiverDevicesWrapper>
            <BlockData>
                <BlockDataStatistics>
                    <TableText><h6>Created</h6></TableText>
                    <TableNumbersText>{created}</TableNumbersText>
                </BlockDataStatistics>
                <BlockDataStatistics>
                    <TableText><h6>In-stock</h6></TableText>
                    <TableNumbersText>{total}</TableNumbersText>
                </BlockDataStatistics>
            </BlockData>
            <BlockData>
                <BlockDataStatistics>
                    <TableText><h6>Ready</h6></TableText>
                    <TableNumbersText>{ready}</TableNumbersText>
                </BlockDataStatistics>
                <BlockDataStatistics>
                    <TableText><h6>Active</h6></TableText>
                    <TableNumbersText>{active}</TableNumbersText>
                </BlockDataStatistics>
                <BlockDataStatistics>
                    <TableText><h6>Disabled</h6></TableText>
                    <TableNumbersText>{disable}</TableNumbersText>
                </BlockDataStatistics>
            </BlockData>
        </ReceiverDevicesWrapper>
    </ReceiverWrapper>
);


export default Acurable;
