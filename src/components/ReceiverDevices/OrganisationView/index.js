import React from 'react';
import Button from '@atlaskit/button';

import {
    Container,
    HeaderWrapper,
    TableText,
    TableNumbersText,
    ReceiverDevicesWrapper,
    BlockData,
    BlockDataStatistics,
    ReceiverWrapper,
} from '../styled';

const Organisation = ({ total, active, disable }) => (
    <ReceiverWrapper>
        <Container>
            <HeaderWrapper><h3>Receiver devices</h3></HeaderWrapper>
            <Button appearance="primary">Manage sensors</Button>
        </Container>
        <ReceiverDevicesWrapper>
            <BlockData>
                <BlockDataStatistics>
                    <TableText><h6>In-stock</h6></TableText>
                    <TableNumbersText>{total}</TableNumbersText>
                </BlockDataStatistics>
                <BlockDataStatistics>
                    <TableText><h6>Active</h6></TableText>
                    <TableNumbersText>{active}</TableNumbersText>
                </BlockDataStatistics>
                <BlockDataStatistics>
                    <TableText><h6>Disabled</h6></TableText>
                    <TableNumbersText>{disable}</TableNumbersText>
                </BlockDataStatistics>
            </BlockData>
        </ReceiverDevicesWrapper>
    </ReceiverWrapper>
);


export default Organisation;
