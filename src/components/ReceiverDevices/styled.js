import styled from 'styled-components';

export const ReceiverWrapper = styled.div`
    display: flex;
    flex-direction: column;
    padding-top: 50px;
`;

export const Container = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
`;

export const HeaderWrapper = styled.div`
    display: flex;
    flex-direction: column;
    font-weight: bold;
`;

export const TitleWrapper = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
`;

export const Title = styled.div`
    display: flex;
    flex-direction: column;
    font-weight: normal;
    font-size: 20px;
    width: 50%;
`;

export const TableText = Title.extend`
    text-align: center;
    font-size: 15px;
    width: auto;
`;

export const TableNumbersText = TableText.extend`
    font-weight: bold;
    font-size: 20px;
`;

export const ReceiverDevicesWrapper = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-between;
`;

export const BlockData = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: space-around;
    width: 50%;
    border: 1px solid #c0c0c0;
    border-radius: 10px;
    padding: 10px;
`;

export const BlockDataStatistics = styled.div`
    display: flex;
    flex-direction: column;
`;
