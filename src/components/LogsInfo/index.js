import React from 'react';
import TableTree, { Headers, Header, Rows, Row, Cell } from '@atlaskit/table-tree';
// import { caption, head, rows } from './content/sample-data';

import {
    HeaderWrapper,
    Wrapper
} from './styled';

const LogsInfo = ({ list, role }) => (
    <Wrapper>
        <HeaderWrapper><h3>Activity log</h3></HeaderWrapper>
        <TableTree>
            <Headers>
                <Header width={300}>Date</Header>
                <Header width={700}>Description</Header>
            </Headers>
            <Rows
                items={list.filter(item => item.user === role)}
                render={({ date, description }) => (
                    <Row itemId={description}>
                        <Cell singleLine>{date}</Cell>
                        <Cell singleLine>{description}</Cell>
                    </Row>
                )}
            />
        </TableTree>
    </Wrapper>
);


export default LogsInfo;
