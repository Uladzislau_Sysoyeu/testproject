import styled from 'styled-components';

export const Wrapper = styled.div`
  margin-top: 20px;
  min-width: 600px;
`;

export const HeaderWrapper = styled.div`
    display: flex;
    flex-direction: column;
    font-weight: bold;
`;