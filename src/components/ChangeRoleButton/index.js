import React from 'react';
import Button from '@atlaskit/button';
import Tooltip from '@atlaskit/tooltip';

const ChangeRoleButton = ({ appearance, changeRole, role }) => (
    <Tooltip content="Change role">
        <Button
            appearance={appearance}
            onClick={changeRole}
        >
            {role}
        </Button>
    </Tooltip>
);


export default ChangeRoleButton;
