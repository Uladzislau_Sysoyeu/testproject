import { dB } from '../../dataBase';


const CONNECT_TO_DB_REQUEST = 'CONNECT_TO_DB_REQUEST';
const CHANGE_ROLE = 'CHANGE_ROLE';

const INITIAL_STATE = {
    error: '',
    collectionsList: {},
    role: '',
    loading: false,
};

export default function reducer(state = INITIAL_STATE, action = {}) {
    switch (action.type) {
        case CONNECT_TO_DB_REQUEST:
            return { ...state, collectionsList: action.payload, error: '', loading: false };
        case CHANGE_ROLE:
            return { ...state, role: action.payload, error: '', loading: false };
        default:
            return state;
    }
}

// <<<ACTIONS>>>
export const requestConnectToDB = () => ({ type: CONNECT_TO_DB_REQUEST, payload: dB() });
export const requestChangeRole = role => ({ type: CHANGE_ROLE, payload: role });
