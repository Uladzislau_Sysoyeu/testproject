import { combineReducers } from 'redux';
import details from './details';

const rootReducer = combineReducers({
    details
});

export default rootReducer;
